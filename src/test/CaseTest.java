package test;

import librairie.Case;
import librairie.Pion;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CaseTest
{
    Case casse;
    Pion pion;
    @Before
    public void setUp() throws Exception
    {
        casse = new Case(1,1);
        pion = new Pion("blanc",1);
    }

    @Test
    public void Test_setPionNonVide()
    {
        casse.setPion(pion);

        assertNotNull(casse.getPion());
    }

    @Test
    public void Test_setPionVide()
    {
        assertNull(casse.getPion());
    }

    @Test
    public void Test_emptyTrue()
    {
        assertEquals(true, casse.empty());
    }

    @Test
    public void Test_emptyFalse()
    {
        casse.setPion(pion);

        assertEquals(false, casse.empty());
    }

    @Test
    public void Test_getPionVide()
    {
        assertNull(casse.getPion());
    }

    @Test
    public void Test_getPionNonVide()
    {
        casse.setPion(pion);
        assertNotNull(casse.getPion());
    }

    @Test
    public void Test_getCaracteristiqueVide()
    {
        assertEquals("",casse.getCaracteristique());
    }

    @Test
    public void Test_getCaracteristiqueNonVide()
    {
        casse.setCaracteristique("En feu");
        assertEquals("En feu",casse.getCaracteristique());
    }

    @Test
    public void Test_getLigne()
    {
        assertEquals(1, casse.getLigne());
    }

    @Test
    public void Test_getColone()
    {
        assertEquals(1,casse.getColone());
    }
}