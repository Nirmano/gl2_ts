package librairie;

public class Main {

    public static void main(String[] args)
    {
        String Joueur1 = "anthony";
        String Joueur2 = "arwen";
        Jeu jeu = new Jeu(Joueur1,Joueur2);

        System.out.println("\nVérification du jeu");
        for (int i = 0; i<2;i++)
        {
            System.out.println("Pion du Joueur "+(i+1));
            jeu.joueur[i].getPion();
        }
    }
}
