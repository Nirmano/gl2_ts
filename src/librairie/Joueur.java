package librairie;

public class Joueur
{
    private String nom;
    private Pion pion [ ];

    public Joueur(String nom, String couleur, Pion pion [ ])
    {
        System.out.println("Nouveau Joueur : " + couleur);
        this.nom = nom;
        this.pion = pion;
    }

    public void getPion()
    {
        for (int i = 0; i < 10; i++)
        {
            if(pion[i].getCapturer())
            {
                System.out.println("Pion "+i+" : Mort");
            }
            else
            {
                System.out.println("Pion "+i+" : Encore en vie");
            }
        }
    }
}
