package librairie;

public class State
{
    private PionState state = new EnAttenteState();

    public void nextState()
    {
        state.next(this);
    }

    public void setState(PionState state)
    {
        this.state = state;
    }
}
