package librairie;

abstract class ACase
{
    String caracteristique;
    boolean vide;
    int ligne;
    int colone;
    Pion pion;

    public boolean empty()
    {
        return vide;
    }

    public int getLigne() {
        return ligne;
    }

    public int getColone() {
        return colone;
    }

    public void setPion(Pion pion)
    {
        if(vide)
        {
            this.pion = pion;
            vide = false;
        }
        else if (this.pion.getCouleur() != pion.getCouleur())
        {
            this.pion.RetirerDuJeu();
            this.pion = pion;
        }
        else
        {
            System.out.println("Cette case est déja occuper par un de vos pions!");
        }
    }

    public Pion getPion()
    {
        if(!vide)
        {
            vide = true;
            Pion temp = pion;
            pion = null;
            return temp;
        }
        else
            return null;
    }

    public void setCaracteristique(String c)
    {
        this.caracteristique = c;
    }

    public String getCaracteristique() { return this.caracteristique; }
}
