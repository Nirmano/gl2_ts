package librairie;

public class Jeu
{
    public Echiquier plateau;
    public Joueur joueur [ ];
    private Pion pionBlanc [ ] ;
    private Pion pionNoir [ ];

    public Jeu(String nomJoueur1, String nomJoueur2)
    {
        joueur = new Joueur[2];
        pionBlanc = new Pion[20];
        pionNoir = new Pion[20];
        System.out.println("Nouveau Jeu :");

        for(int i = 0; i < 20; i++)
        {
            pionBlanc[i] = new Pion("blanc",1);
        }

        for(int i = 0; i < 20; i++)
        {
            pionNoir[i] = new Pion("noir",1);
        }

        joueur[0]  = new Joueur(nomJoueur1,"blanc", pionBlanc);
        joueur[1] = new Joueur(nomJoueur2,"noir", pionNoir);

        plateau = new Echiquier(pionNoir,pionBlanc);
    }
}
