package librairie;

public class SelectionnerState implements PionState
{
    @Override
    public void next(State state)
    {
        state.setState(new EnDeplacementState());
    }
}