package librairie;

public class EnAttenteState implements PionState
{
    @Override
    public void next(State state)
    {
        state.setState(new SelectionnerState());
    }
}
