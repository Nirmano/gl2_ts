package librairie;

public class Echiquier
{
    Case casse [ ] [ ];
    Pion pionNoir[];
    Pion pionBlanc[];

    public Echiquier(Pion noir[], Pion blanc[])
    {
        pionBlanc = blanc;
        pionNoir = noir;
        casse = new Case[10][10];
        System.out.println("Nouveau Plateau :");
        for(int i = 0; i < 10; i++)
        {
            for(int j = 0; j < 10; j++)
            {
                casse[i][j] = new Case(i,j);
            }
        }

        //**********
        //Placement des pions noirs sur les cases
        //**********
        int index = 0;
        for (int i = 0;i < 4 ;i+=2)
        {
            for (int j = 1; j < 10; j +=2 )
            {
                casse[i][j].setPion(pionNoir[index]);
                index++;
            }
            for (int j = 0; j < 10; j +=2 )
            {
                casse[i+1][j].setPion(pionNoir[index]);
                index++;
            }
        }

        //**********
        //Placement des pions blancs sur les cases
        //**********
        index = 0;

        for (int i = 9;i > 5 ;i-=2)
        {
            for (int j = 0; j < 10; j +=2 )
            {
                casse[i][j].setPion(pionBlanc[index]);
                index++;
            }
            for (int j = 1; j < 10; j +=2 )
            {
                casse[i-1][j].setPion(pionBlanc[index]);
                index++;
            }
        }
    }

    public Case getCase(int i, int j)
    {
        return casse[i][j];
    }
}