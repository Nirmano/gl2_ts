package librairie;

public class EnDeplacementState implements PionState
{
    @Override
    public void next(State state)
    {
        state.setState(new EnAttenteState());
    }
}