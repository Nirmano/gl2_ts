package librairie;

public class Pion extends APiece
{
    public Pion(String couleur, int mouvement)
    {
        this.couleur = couleur;
        System.out.println("Nouveau Pion : "+this.couleur);
        this.mouvement = mouvement;
        SurLeJeu();
        state = new State();
    }

    @Override
    public void Selectionner()
    {
        selectionner = true;
        state.nextState();
        EnDeplacement();
    }

    @Override
    public void EnDeplacement()
    {
        state.nextState();
        EnAttente();
    }
}
