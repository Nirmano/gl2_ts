package librairie;

abstract class APiece
{
    protected String couleur;
    protected boolean capturer;
    protected boolean selectionner;
    protected int mouvement;
    protected State state;

    public void EnAttente()
    {
        state.nextState();
        selectionner = false;
    }
    public void SurLeJeu(){
        capturer = false;
        selectionner = false;
    }
    abstract void Selectionner();
    abstract void EnDeplacement();
    public void RetirerDuJeu()
    {
        capturer = true;
    }

    public boolean getCapturer() { return capturer; }
    public void setCapturer(boolean c) {this.capturer = c; }

    public boolean getSelect() { return selectionner; }

    public String getCouleur() { return couleur; }
}
